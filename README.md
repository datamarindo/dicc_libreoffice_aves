# Diccionario de aves mexicanas para LibreOffice en Linux

Añade todos los __1,119__ nombres científicos, nombres en inglés y nombres en español según Berlanga et al., 2019, al diccionario de LibreOffice para que reconozca las palabras y corrija en casos de mala escritura. _El diccionario está separado género y especie y nombres comunes, por lo cual son 3,000 palabras en total_

![nombres incorrectos resaltados]( Screenshot_from_2020-05-11_22-28-05.png)
# Para instalarlo:

Copiar la siguiente línea en terminal:

```wget https://gitlab.com/datamarindo/dicc_libreoffice_aves/-/raw/master/crea_diccionario_aves_mexicanas_libreoffice.sh && chmod +x crea_diccionario_aves_mexicanas_libreoffice.sh && ./crea_diccionario_aves_mexicanas_libreoffice.sh ```

Para revisar, la línea:

```cat ~/.config/libreoffice/4/user/wordbook/standard.dic```

imprimiría
```
OOoUserDict1
lang: <none>
type: positive
---
Tinamus
major
Crypturellus
soui
cinnamomeus
```


__el script respeta las adiciones previas al diccionario__

Referencia:
Berlanga,  H.,  H.  Gómez  de  Silva,  V.  M.  Vargas-Canales,  V.  Rodríguez-Contreras,  L.  A. Sánchez-González,  R. Ortega-Álvarez  y  R.  Calderón-Parra  (2019). Aves  de  México:  Lista actualizada de especies y nombres comunes. CONABIO, México D.F.
https://www.biodiversidad.gob.mx/media/1/ciencia-ciudadana/documentos/Lista_actualizada_aos_2019.pdf