#!/bin/bash
FILE=~/.config/libreoffice/4/user/wordbook/standard.dic
DIRECTORY=~/.config/libreoffice/4/user/wordbook/
if [ -f $FILE ];
    then
        curl https://gitlab.com/datamarindo/dicc_libreoffice_aves/-/raw/master/standard.dic >> ~/.config/libreoffice/4/user/wordbook/standard.dic
    else
        if [ -d $DIRECTORY ];
        then
            echo -e "OOoUserDict1\nlang: <none>\ntype: positive\n---" > ~/.config/libreoffice/4/user/wordbook/standard.dic        
            curl https://gitlab.com/datamarindo/dicc_libreoffice_aves/-/raw/master/standard.dic >> ~/.config/libreoffice/4/user/wordbook/standard.dic 
        else
            mkdir ~/.config/libreoffice/4/user/wordbook/
            echo -e "OOoUserDict1\nlang: <none>\ntype: positive\n---" > ~/.config/libreoffice/4/user/wordbook/standard.dic        
            curl https://gitlab.com/datamarindo/dicc_libreoffice_aves/-/raw/master/standard.dic ~/standard.dic >> .config/libreoffice/4/user/wordbook/standard.dic 
        fi
fi
